use std::env;
use std::fs::File;
use std::io::Read;
use std::process;
use std::sync::mpsc::{self, Sender};
use std::{thread, time};


fn init_file(fd: &Option<String>) -> File {
    match fd {
        Some(fd) => match File::open(fd) {
            Ok(f) => return f,
            Err(e) => {
                println!("Error: {}", e);
                process::exit(1);
            }
        },
        None => {
            println!("Need Path/File");
            process::exit(1);
        }
    };
}


fn live_log(mut fd: File, tx: Sender<String>) {
    let mut content = String::new();
    let ten_millis = time::Duration::from_millis(10);

    fd.read_to_string(&mut content).unwrap();

    loop {
        let mut content1 = String::new();
        fd.read_to_string(&mut content1).unwrap();
        if content1 != content {
            for line in content.lines() {
                tx.send(line.to_string()).unwrap();
            }
            content = String::from(content1);
        }
        thread::sleep(ten_millis);
    }
}


fn main() {
    let mut logfile = init_file(&env::args().nth(1));
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        live_log(logfile, tx);
    });

    loop {
        match rx.recv() {
            Ok(line) => println!("{}", line),
            Err(e) => {
                println!("Error: {}", e);
                process::exit(1);
            }
        }
    }
}
